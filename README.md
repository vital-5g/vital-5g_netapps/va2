# Distributed sensor data ingestion fusion and post-processing

## Description
VA2 supports data ingestion, fusion and post-processing functionalities from distributed data sources. 
The components and architecture of VA2 have been described in detail in [Deliverable 2.1](https://www.vital5g.eu/wp-content/uploads/2022/01/VITAL5G_D2.1_Initial_NetApps_blueprints_and_Open_Repository_design_Final.pdf). 


These components and the early drop features are graphically displayed in the following Figure:

![VA2 High-Level Architecture](doc/VA2_high-level_architecture.png)

Functionalities include:
1) Testbed collector – from UC2: Performs ingestion of data from various sources, whether one or multiple T&L facilities or external sources. In this early drop we ingest data from sources (IoT devices) specific to use case 2.

2) Data Collection component: The data collection component is responsible for the collection of data through the southbound interfacing (via connectors). Its full version will have capabilities for acquisition of both streaming and batch data through HTTP REST APIs querying directly the data source or through message bus services, at various rates. For this early drop, the following features will be available:

	* External endpoint to data collection: The appropriate connectors have been developed that will ensure the acquisition of batch testbed data through HTTP REST APIs from the data sources available at this stage.
	* Internal endpoint to Data Persistence module: The collected data are then ingested in a distributed filesystem which will allow for storage of structured and unstructured data (more specifically, Apache HDFS storage layer).

## Installation
Installation instructions for:
- **Data collection and Data Persistance** are detailed in this link: [beia/iot-stack/README.md](beia/iot-stack/README.md)
- **Data ingestion from InfluxDB to Apache Hive** are detailed in this link: [incelligent/influx-spark-ingestion/README.md](incelligent/influx-spark-ingestion/README.md)
- A **sample data generator** are detailed on this link: [beia/dummy-data-generator/README.md](beia/dummy-data-generator/README.md)

## Support
For support, contact [info@incelligent.net](mailto:info@incelligent.net) / [proiecte@beia.ro](mailto:proiecte@beia.ro).

## Authors and acknowledgment
This is the work of Incelligent P.C. and BEIA CONSULT INTERNATIONAL SRL's  personnel in the context of the VITAL-5G Project.

## Disclaimer
The complete functionality of the Network Application leverages closed-source components developed by INCE. In this repository we provide deprecated code of the open-source components developed by BEIA and INCE

## License
[GNU AGPLv3](https://choosealicense.com/licenses/agpl-3.0/)
