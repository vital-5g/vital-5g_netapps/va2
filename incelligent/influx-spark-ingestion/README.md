# vital5g-influx-hive-ingestion

requirements package:

    influxdb-client
    pandas
    json




# Influxdb_v1 
Steps:
1) create the Influxdb & grafana:

		cd influx1.8
		docker-compose up -d

2) Insert data:
		
* create data base:

		
			curl -i -XPOST http://localhost:8086/query --data-urlencode "q=CREATE DATABASE mydb"

* insert data:


		curl -i -XPOST 'http://localhost:8086/write?db=mydb' --data-binary 'cpu_load_short,host=server02 value=0.67
			cpu_load_short,host=server02,region=us-west value=0.55 1422568543702900257
			cpu_load_short,direction=in,host=server01,region=us-west value=2.0 1422568543702900257'

* read data:


		curl -G 'http://localhost:8086/query?pretty=true' --data-urlencode "db=mydb" --data-urlencode "q=SELECT * FROM \"cpu_load_short\" "


3) read data with Python API:
	
		from influxdb import InfluxDBClient
		import pandas as pd
		# You can generate an API token from the "API Tokens Tab" in the UI
		client = InfluxDBClient('localhost', 8086, 'admin', 'admin', 'mydb')
		tableName= 'cpu_load_short'
		q= f'select * from {tableName};'
		query = client.query(q)
		df = pd.DataFrame(query.get_points() )
		df.to_csv(f'{tableName}.csv',index=False) 


4) ingest data script: <b>ingestion_v1.py</b>

# Influxdb_v2

Steps:
1) create the Influxdb & grafana:

		cd influx2
		docker network create -d bridge monitoring
		sudo docker-compose up -d
 
2) Grafana Add new source:

 			user:admin
 			pass:admin
 		1) http://localhost:3000/
 		2)  add influxDB: 
 			extra settings: Add database: e.x. grafana
 							Add user: grafana
 							HTTP Method: Get
 			Save & test
 		3) Error -> (InfluxDB Error: Bad Gateway) 
 			use this command: 

 				sudo firewall-cmd --permanent --zone=public --add-port=8086/tcp
 				sudo firewall-cmd --reload
 		Save & test
        

3) Open in chrome: http://localhost:8086/

		a.  create a initial user
		b.  create a bucket 'financials' 
		c.  in 'Influx Home page'>> 'Load Data'>> 'Buckets'>> 'financials'>> 'Add Data' >> 'Line Protocol' >> Upload 5-10 lines from sample1.csv

4) ingest data script: <b>ingestion_v2.py</b>

	replace the token with yours: "9RC2K7jmOGuTguPX2mVrLLOofqqNrCaI7wjxV_bV2ya76agahC3kF0_YVVgGc55-_O9PIZ0-qFIfeJMHJYrQwQ=="
	

#	InfluxDb to Hive Pipeline

1) Influxdb_v1 & Influxdb_v2 are exporting data to local csv 
2) using 'scp', we are copy the data in the hadoop node (maybe edge-node)
3) upload csv into hdfs
4) create the ddl of the hive table   

