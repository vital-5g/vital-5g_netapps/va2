################################
#                              #
#     InfluxDB 1.8             #
#                              #
#                              #
################################

from influxdb import InfluxDBClient
import pandas as pd
import json

# You can generate an API token from the "API Tokens Tab" in the UI
json_path = 'influx1.8/config.json'
with open(json_path) as f:
    influxDB = json.load(f)
 

# You can generate an API token from the "API Tokens Tab" in the UI
client = InfluxDBClient(influxDB['url'], influxDB['port'], influxDB['user'], influxDB['pass'], influxDB['db'])
tableName= influxDB['table']
q= f'select * from {tableName};'
query = client.query(q)
df = pd.DataFrame(query.get_points() )
df.to_csv(f'{tableName}.csv',index=False) 