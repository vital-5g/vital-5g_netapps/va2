################################
#                              #
#     InfluxDB 2               #
#                              #
#                              #
################################


from datetime import datetime

from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS
import json
import pandas as pd

# You can generate an API token from the "API Tokens Tab" in the UI
json_path = 'influx2/config.json'
with open(json_path) as f:
    influxDB = json.load(f)
    print(influxDB)
 

# number of tables
# def influxDB_partition_num(influxDB = influxDB):
#     with InfluxDBClient(url=influxDB["url"], token=influxDB["token"], org=influxDB["org"]) as client:
#         tables = client.query_api().query(influxDB["query"], org = influxDB["org"])
#         print(tables)
#         table_size = len(tables)
#         client.close()
#     return table_size

# x = influxDB_partition_num(influxDB)

#return the table

def influxDB_partition(influxDB = influxDB):
    records = []
    with InfluxDBClient(url=influxDB["url"], token=influxDB["token"], org=influxDB["org"]) as client:
        tables = client.query_api().query(influxDB["query"], org = influxDB["org"])        
        records = []
        for table in tables:
            for record in table.records:
                records.append((record.get_time(), record.get_field(), record.get_value()))
        client.close()
        df = pd.DataFrame(records, columns=['time', 'column', 'value'])
        df = df.pivot_table(index=['time'], columns='column', values='value', aggfunc='first')
        return df


recordsDF = influxDB_partition( influxDB)

recordsDF.to_csv(f'export_influxDB.csv',index=False) 
